import matplotlib.pyplot as plt
from matplotlib.widgets import RectangleSelector
import numpy as np
from .analyser.analyser import Analyser


class InteractiveSelectAndAnalyse:

    def __init__(self, analyser: Analyser, func_analysis_preview=None, func_accepted_results_preview=None):
        self.analyser = analyser
        self.func_analysis_preview = func_analysis_preview
        self.func_accepted_results_preview = func_accepted_results_preview
        
        self.figure = plt.figure()
        self.ax = self.figure.add_subplot(1, 1, 1)
        self.ax.grid()

        self.line_data = self.ax.plot([], [], 'o')[0]
        self.line_selected = self.ax.plot([], [], 'o')[0]
        self.line_ana_preview = self.ax.plot([], [])[0]
        self.line_accepted_results = self.ax.plot([], [], 'o')[0]

        self.rs = RectangleSelector(self.ax,
                                    self._rectangle_selector_callback,
                                    drawtype='box',
                                    useblit=True,
                                    interactive=False)

        plt.connect("key_press_event", self._key_press_handler)

        self._last_result = None
        self._current_results = []
        self._results = []
        self._current_index = None
        self._selected_xrange = None

    def get_results(self):
        return self._results

    def run(self, datasets):
        self._datasets = datasets
        self._results = []
        
        self._current_index = 0
        self._draw_current_dataset()
        self._update_plot()
        plt.show()

    def _key_press_handler(self, event):
        """
        Handle keyboard events.
        
        a/A:  Accept current result. 
        d/D:  Delete last result
        n/N:  Show next dataset.
        """
        if event.key in ['a', 'A']:
            self._current_results.append(self._last_result)
            self._clear_selected_and_preview_lines()
            self._draw_accepted_results()
            self._update_plot()
        elif event.key in ['d', 'D']:
            self._current_results.pop()
            self._draw_accepted_results()
            self._update_plot()
        elif event.key in ['n', 'N']:
            self._results.append(self._current_results)
            self._last_result = None
            self._current_results = []
            self._current_index += 1
            self._clear_all_lines()
            self._draw_current_dataset()
            self._update_plot()
        
    def _rectangle_selector_callback(self, event_click, event_release):
        x1 = event_click.xdata
        x2 = event_release.xdata

        self._selected_xrange = (x1, x2)

        data = self._datasets[self._current_index]
        if data.ndim == 2:
            x = data[:, 0]
            y = data[:, 1]
        else:
            x = np.arange(data.shape[0]),
            y = data
        
        xslice = x[(x > x1) & (x < x2)]
        yslice = y[(x > x1) & (x < x2)]

        self.line_selected.set_data(xslice, yslice)
        self._last_result = self.analyser(np.array([xslice, yslice]).transpose())
        self._draw_preview()
        self._update_plot()
        
    def _clear_selected_and_preview_lines(self):
        self.line_selected.set_data([], [])
        self.line_ana_preview.set_data([], [])        

    def _clear_all_lines(self):
        self._clear_selected_and_preview_lines()
        self.line_accepted_results.set_data([], [])
        self.line_data.set_data([], [])        

    def _draw_accepted_results(self):
        if self.func_accepted_results_preview is None:
            return
        xdata, ydata = self.func_accepted_results_preview(self._current_results)
        self.line_accepted_results.set_data(xdata, ydata)

    def _draw_preview(self):
        if self.func_analysis_preview is None:
            return
        xdata, ydata = self.func_analysis_preview(self._last_result, self._selected_xrange)
        self.line_ana_preview.set_data(xdata, ydata)
        
    def _draw_current_dataset(self):
        if self._current_index >= len(self._datasets):
            plt.close(self.figure)
            return
        data = self._datasets[self._current_index]
        if data.ndim == 2:
            self.line_data.set_data(data[:, 0], data[:, 1])
        else:
            self.line_data.set_data(np.arange(data.shape[0]), data)

    def _update_plot(self):
        self.ax.relim(visible_only=True)
        self.ax.autoscale()
        self.figure.canvas.draw()
