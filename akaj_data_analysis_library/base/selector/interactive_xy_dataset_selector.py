import matplotlib.pyplot as plt
import numpy as np
from .xy_dataset_selector import XYDatasetSelector


class InteractiveXYDatasetSelector(XYDatasetSelector):

    def __init__(self, plot_args=[], plot_kwargs={}):
        super().__init__()
        self.figure = plt.figure()
        self.ax = self.figure.add_subplot(1, 1, 1)        
        self.line = self.ax.plot([], [], *plot_args, **plot_kwargs)[0]
        self.current_index = 0
        plt.connect("key_press_event", self._key_press_handler)
        
    def _start_selection(self, data_collection):
        self.data_collection = data_collection
        self._draw_current_dataset()                        
        plt.show()     
        
    def _draw_current_dataset(self):
        if self.current_index >= len(self.data_collection):
            plt.close(self.figure)
            return
        data = self.data_collection[self.current_index]
        if data.ndim == 2:
            self.line.set_data(data[:, 0], data[:, 1])
        else:
            self.line.set_data(np.arange(data.shape[0]), data)
        self.ax.set_title("i = {}".format(self.current_index))
        self.ax.relim()
        self.ax.autoscale()        
        self.figure.canvas.draw()

    def _key_press_handler(self, event):
        if event.key == 'h':
            self._toggle_help_texts()
            return
        if event.key == 'y':
            self.selected.append(self.current_index)
            self.current_index += 1
            self._draw_current_dataset()
        elif event.key == 'n':
            self.current_index += 1
            self._draw_current_dataset()
