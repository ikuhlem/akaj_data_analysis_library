import numpy as np
from typing import Any

DataCollection = Any
SelectionMask = np.ndarray

class Selector:

    def __init__(self):
        self._validators = [] # type: List[Validator]

    def _start_selection(self, input_collection: DataCollection):
        raise NotImplementedError("Override in derived class!")

    def _selected_to_selection_mask(self) -> SelectionMask:
        raise NotImplementedError("Override in derived class!")

    def select(self, input_collection: DataCollection) -> SelectionMask:
        self._validate(input_collection)
        self.selected = []
        self._start_selection(input_collection)
        return self._selected_to_selection_mask()

    def _validate(self, input_collection: DataCollection):
        for v in self._validators:
            v(input_collection)            

    def __call__(self, input_collection: DataCollection) -> SelectionMask:
        return self.select(input_collection)
