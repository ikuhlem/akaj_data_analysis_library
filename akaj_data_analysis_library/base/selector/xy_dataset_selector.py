import numpy as np
from .selector import Selector, SelectionMask
from ..validator.xy_dataset_collection_validator import XYDatasetCollectionValidator


class XYDatasetSelector(Selector):

    def __init__(self):
        super().__init__()
        self._validators.append(XYDatasetCollectionValidator())

    def _selected_to_selection_mask(self) -> SelectionMask:
        return np.array(self.selected, dtype=int)


