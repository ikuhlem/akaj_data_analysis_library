import numpy as np
from .validator import Validator

class XYDatasetValidator(Validator):

    def validate(self, dataset):
        if not isinstance(dataset, np.ndarray):
            raise TypeError("dataset (type: {}) has to be numpy.ndarray! ".format(type(dataset)))
        if dataset.ndim > 2:
            raise ValueError("dataset has to be 1D or 2D-array.")
        if dataset.ndim == 2:
            if dataset.shape[1] != 2:
                msg = "In case of 2D-array, dataset has to have "
                msg += "exactly 2 columns (i.e. dataset.shape[1] == 2), "
                msg += "where the first column is used as x-axis "
                msg += "and the second as y-axis."
                
                raise ValueError(msg)
