from typing import Any

Data = Any

class Validator:

    def __init__(self):
        pass

    def validate(self, data: Data):
        """
        This should raise an TypeError or ValueError if data is invalid.
        """
        raise NotImplementedError("Override in derived class!")

    def __call__(self, data: Data):
        return self.validate(data)
