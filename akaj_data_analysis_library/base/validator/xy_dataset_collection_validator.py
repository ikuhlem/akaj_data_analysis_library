import numpy as np
from .validator import Validator
from .xy_dataset_validator import XYDatasetValidator

class XYDatasetCollectionValidator(Validator):

    def __init__(self):
        super().__init__()
        self.xy_dataset_validator = XYDatasetValidator()

    def validate(self, dataset_collection):       
        if isinstance(dataset_collection, list) or isinstance(dataset_collection, np.ndarray):
            for dataset in dataset_collection:
                self.xy_dataset_validator(dataset)
            return
        raise TypeError("dataset_collection needs to be a list (of 2D-arrays) or a 3D-array.")


