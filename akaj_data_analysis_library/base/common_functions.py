import numpy as np

def sigmoid_function(x, L, k, x0, C):    
    return L / (1. + np.exp(-k * (x - x0))) + C

def initial_params_step_sigmoid(x, y):
    if y[:len(x)//2].mean() < y[len(x)//2:].mean():
        k0 = 100
    else:
        k0 = -100
    return [y.max()-y.min(), k0,
            0.5*(x.min() + x.max()), x.min()]
