import numpy as np
from .analyser import Analyser
from ..validator.xy_dataset_validator import XYDatasetValidator


class PolyFitter(Analyser):

    def __init__(self, degree):
        super().__init__()
        self.degree = degree
        self._validators.append(XYDatasetValidator())

    def _do_analyse(self, data):
        if data.ndim == 1:
            x = np.arange(data.shape[0])
            y = data
        else:
            x = data[:, 0]
            y = data[:, 1]
        self._result = np.polyfit(x, y, self.degree)        
