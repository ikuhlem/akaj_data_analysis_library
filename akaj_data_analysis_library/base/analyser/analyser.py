from typing import Any

Result = Any

class Analyser:

    def __init__(self):
        self._validators = [] # type List[Validator]

    def _do_analyse(self, data) -> None:
        raise NotImplementedError("Override in derived class!")

    def analyse(self, data) -> Result:
        self._validate(data)
        self._result = None # type: Result
        self._do_analyse(data)
        return self._result
    
    def _validate(self, data):
        for v in self._validators:
            v(data)

    def __call__(self, data) -> Result:
        return self.analyse(data)
