import numpy as np
from scipy.optimize import curve_fit
from inspect import signature
from .analyser import Analyser
from ..validator.xy_dataset_validator import XYDatasetValidator


class CurveFitter(Analyser):

    def __init__(self, curve_function, initial_params_function=None):
        super().__init__()        
        self._validators.append(XYDatasetValidator())
        self.curve_function = curve_function
        self.initial_params_function = initial_params_function

    def _do_analyse(self, data):
        if data.ndim == 1:
            x = np.arange(data.shape[0])
            y = data
        else:
            x = data[:, 0]
            y = data[:, 1]
        if self.initial_params_function is not None:
            p0 = self.initial_params_function(x, y)
        else:
            sig = signature(self.curve_function)
            p0 = [1]*(len(sig.parameters) - 1)
        try:
            self._result = curve_fit(self.curve_function, x, y, p0)
        except RuntimeError:
            p0[1] = -p0[1]
            self._result = curve_fit(self.curve_function, x, y, p0)    
