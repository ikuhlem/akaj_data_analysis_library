from typing import Callable, Union, Tuple
import matplotlib.pyplot as plt

ParamRange = Union[Tuple[int, int],
                   Tuple[float, float]]

class InteractiveParameterRangeFinder:

    def __init__(self, data_generator: Callable, plot_generator: Callable):
        self._data_generator = data_generator
        self._plot_generator = plot_generator

        self.selected_range = None
        self.fig = None
        self.ax = None

        self._currently_looking_for = "lower-limit"
        self._current_parameter = None
        self._history = []
        self._max = None
        self._min = None
        self._smallest_increment = None

    def find_range(
            self,
            param_range: ParamRange,
            smallest_increment: Union[int, float]
    ) -> ParamRange:
        self.selected_range = [param_range[1], param_range[0]]
        self._max = param_range[1]
        self._min = param_range[0]
        self._smallest_increment = smallest_increment
        self._setup_axis()
        raise NotImplementedError
        # return tuple(self.selected_range)


    def _setup_axis(self):
        self.fig, self.ax = plt.subplots(1, 1)
        plt.connect("key_press_event", self._key_press_handler)

    def _key_press_handler(self, event):
        if event.key == 'h':
            self._toggle_help_texts()
        if event.key == 'y':
            self._handle_accept_current_parameter()
        if event.key == 'n':
            self._handle_reject_current_parameter()
        if event.key == 'z':
            self._handle_go_back()

    def __call__(
            self,
            param_range: ParamRange,
            smallest_increment: Union[int, float]
    ) -> ParamRange:
        return self.find_range(param_range, smallest_increment)

    def _toggle_help_texts(self):
        print("toggle help texts not implemented yet")

    def _handle_accept_current_parameter(self):
        self._history.append((self._currently_looking_for, self._current_parameter))
        if self._currently_looking_for == 'lower-limit':
            if self._current_parameter < self.selected_range[0]:
                self.selected_range[0] = self._current_parameter
        elif self._currently_looking_for == 'upper-limit':
            if self._current_parameter > self.selected_range[1]:
                self.selected_range[1] = self._current_parameter
        else:
            raise RuntimeError("currently_looking_for is neither upper nor lower limit")
        self._go_to_next_parameter_after_accept()


    def _handle_reject_current_parameter(self):
        self._history.append((self._currently_looking_for, self._current_parameter))
        self._go_to_next_parameter_after_reject()

    def _handle_go_back(self):
        raise NotImplementedError

    def _go_to_next_parameter_after_accept(self):
        if self._currently_looking_for == 'upper-limit':
            next_parameter = (self._current_parameter + self._max) / 2
            if next_parameter - self._current_parameter > self._smallest_increment:
                self._current_parameter = next_parameter
                return
            self._currently_looking_for = None
            return
        if self._currently_looking_for == 'lower-limit':
            next_parameter = (self._current_parameter + self._min) / 2
            if self._current_parameter - next_parameter > self._smallest_increment:
                self._current_parameter = next_parameter
                return
            self._currently_looking_for = 'upper-limit'
            self._current_parameter = (self._max + self._min) / 2
        raise RuntimeError("currently_looking_for is neither upper nor lower limit")


    def _go_to_next_parameter_after_reject(self):
        raise NotImplementedError


