from .step_detector_base import StepDetectorBase

class FitStepDetector(StepDetectorBase):

    def __init__(self):
        super(FitStepDetector, self).__init__()
        raise NotImplementedError("")

    def _detect_steps_single_dataset(self):
        raise NotImplementedError("")
