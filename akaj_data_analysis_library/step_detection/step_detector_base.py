from typing import Union, List
import numpy as np
from .detected_steps import DetectedSteps


XYDataCollection = Union[np.ndarray, List[np.ndarray]]
DetectedStepsCollection = Union[DetectedSteps, List[DetectedSteps]]

class StepDetectorBase:

    def __init__(self):
        pass

    def detect_steps(self, data: XYDataCollection) -> DetectedStepsCollection:
        if isinstance(data, list):
            detected_steps_collection = []
            for d in data:
                assert d.ndim == 2
                detected_steps_collection.append(self._detect_steps_single_dataset(d))
            return detected_steps_collection
        assert data.ndim == 2
        return self._detect_steps_single_dataset(data)

    def _detect_steps_single_dataset(self, dataset):
        raise NotImplementedError("Abstract base class! Override this method in child classes.")
