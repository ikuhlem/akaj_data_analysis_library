import numpy as np

from akaj_data_analysis_library.base.selector.interactive_xy_dataset_selector import InteractiveXYDatasetSelector

def test_select_from_5_datasets():
    a = np.random.rand(5, 1000, 2)
    sel = InteractiveXYDatasetSelector()
    r = sel(a)
