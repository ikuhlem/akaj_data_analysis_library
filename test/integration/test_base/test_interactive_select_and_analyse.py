import numpy as np
from akaj_data_analysis_library.base.interactive_select_and_analyse import InteractiveSelectAndAnalyse
from akaj_data_analysis_library.base.analyser.poly_fitter import PolyFitter
from akaj_data_analysis_library.base.analyser.curve_fitter import CurveFitter
from akaj_data_analysis_library.base.common_functions import (sigmoid_function,
                                                              initial_params_step_sigmoid)


def test_interactive_baseline_fit():

    # -------------------------------------------------------------
    #       function to generate data for lin fit preview
    # -------------------------------------------------------------
    def draw_lin_fit(lin_fit_params, xlim):
        x = np.linspace(xlim[0], xlim[1], 1000)
        y = x*lin_fit_params[0] + lin_fit_params[1]
        return x, y

    # -------------------------------------------------------------
    #           generate some data
    # -------------------------------------------------------------
    datasets = []
    for i in range(5):
        ds = np.zeros(((i+1)*200, 2))
        ds[:, 0] = np.random.rand((i+1)*200) * (i+2)
        ds[:, 1] = ds[:, 0] ** 2 + i + np.random.randn((i+1) * 200)
        datasets.append(ds)

    # -------------------------------------------------------------
    #           create linear fitter
    # -------------------------------------------------------------
    lin_fitter = PolyFitter(1)

    # -------------------------------------------------------------
    #           create and run interactive tool
    # -------------------------------------------------------------
    isa = InteractiveSelectAndAnalyse(lin_fitter, draw_lin_fit)
    isa.run(datasets)
    print(isa.get_results())


def test_interactive_step_fit():    

    # -------------------------------------------------------------
    #       function to generate data for lin fit preview
    # -------------------------------------------------------------
    def draw_sigmoid_fit(fit_params, xlim):
        x = np.linspace(xlim[0], xlim[1], 1000)
        y = sigmoid_function(x, *fit_params[0])
        return x, y

    # -------------------------------------------------------------
    #    function to generate data for selected steps preview
    # -------------------------------------------------------------
    def draw_selected_steps(fit_results):
        x = []
        y = []
        for fr in fit_results:
            x.append(fr[0][2])
            y.append(fr[0][3] + 0.5 * fr[0][0])
        return np.array(x), np.array(y)

    # -------------------------------------------------------------
    #           generate some data
    # -------------------------------------------------------------
    datasets = []
    for i in range(5):
        ds = np.zeros(((i+1)*200, 2))
        ds[:, 0] = np.random.rand((i+1)*200) * (i+2)
        ds[(ds[:, 0] > 0.5) & (ds[:, 0] < 1.50), 1] = 2
        ds[ds[:, 0] >= 1.50, 1] = -2
        ds[:, 1] += np.random.randn((i+1) * 200)
        datasets.append(ds)
    
    # -------------------------------------------------------------
    #           create sigmoid fitter
    # -------------------------------------------------------------
    sigm_fitter = CurveFitter(sigmoid_function, initial_params_step_sigmoid)

    # -------------------------------------------------------------
    #           create and run interactive tool
    # -------------------------------------------------------------
    isa = InteractiveSelectAndAnalyse(sigm_fitter, draw_sigmoid_fit,
                                      draw_selected_steps)
    isa.run(datasets)
    print([r[0] for r in isa.get_results()])
