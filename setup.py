"""
USAGE: 
   o install in develop mode: navigate to the folder containing this file,
                              and type 'python setup.py develop --user'.
                              (ommit '--user' if you want to install for 
                               all users)                           
"""


from setuptools import setup
# HOME=os.path.expanduser("~")


## use setup to install SDF package and executable scripts
setup(name='akaj_data_analysis_library',
      version='0.0.1',
      description='Collection of data analysis snippets.',
      url='',
      author='Ilyas Kuhlemann',
      author_email='ilyasp.ku@gmail.com',
      license='MIT',
      packages=['akaj_data_analysis_library'],
      entry_points={
          "console_scripts": [
          ],
          "gui_scripts": [
          ]
      },
      install_requires=['numpy',
                        'matplotlib',
                        'scipy'],
      zip_safe=False)
